from django.shortcuts import redirect

def dietitian_cookie_required(view_func):
    def wrapper(request, *args, **kwargs):
        dietitian_id = request.COOKIES.get('dietitian')
        if not dietitian_id:
            return redirect('/dietitian/login/')
        return view_func(request, *args, **kwargs)
    return wrapper
