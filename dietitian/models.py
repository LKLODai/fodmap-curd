from django.db import models
import uuid

# Create your models here.
# the length of CharField might need to change
class Dietitian(models.Model):
    GENDER_CHOICE = [
        ("MALE", "MALE"),
        ("FEMALE", "FEMALE"),
        ("UNKNOWN", "UNKNOWN")
    ]
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    first_name = models.CharField(max_length=128)
    middle_name = models.CharField(max_length=128, blank=True, null=True)
    last_name = models.CharField(max_length=128)
    email = models.CharField(max_length=320)
    telephone_prefix = models.CharField(max_length=16)
    phone_number = models.CharField(max_length=128)
    country_code_ios3 = models.CharField(max_length=3)
    gender = models.CharField(max_length=16, choices=GENDER_CHOICE)
    is_active = models.BooleanField(default=False)
    creation_time = models.DateTimeField(auto_now=True)
    modified_on = models.DateTimeField(auto_now=True)
    modified_by = models.CharField(max_length=128, blank=True, null=True)
    password = models.CharField(max_length=128)
    description = models.JSONField(blank=True, null=True)


class PracticeLocation(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    dietitian = models.ForeignKey(Dietitian, on_delete=models.CASCADE)
    line1 = models.CharField(max_length=128)
    line2 = models.CharField(max_length=128, blank=True, null=True)
    street = models.CharField(max_length=128)
    city = models.CharField(max_length=128)
    country = models.CharField(max_length=128)
    suburb = models.CharField(max_length=128, blank=True, null=True)
    postcode = models.CharField(max_length=128)
    website = models.CharField(max_length=128)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    is_active = models.BooleanField(default=False)
    creation_time = models.DateTimeField(auto_now=True)
    modified_on = models.DateTimeField(auto_now=True)
    modified_by = models.CharField(max_length=128, blank=True, null=True)
    description = models.JSONField(blank=True, null=True)
