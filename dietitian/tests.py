from django.test import TestCase
from .models import Dietitian

class DietitianModelTest(TestCase):

    def setUp(self):
        self.dietitian = Dietitian.objects.create(
            first_name="John",
            last_name="Doe",
            email="john.doe@example.com",
            telephone_prefix="+1",
            phone_number="1234567890",
            country_code_ios3="AUS",
            gender="male",
            password="hashed_password"
        )

    def test_dietitian_creation(self):
        self.assertEqual(self.dietitian.first_name, "John")
        self.assertEqual(self.dietitian.last_name, "Doe")
