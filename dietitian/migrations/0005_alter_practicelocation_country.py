# Generated by Django 4.2.13 on 2024-05-18 13:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dietitian', '0004_practicelocation_country'),
    ]

    operations = [
        migrations.AlterField(
            model_name='practicelocation',
            name='country',
            field=models.CharField(max_length=128),
        ),
    ]
