from dietitian.models import Dietitian
from django import forms
import bcrypt


class DietitianAddForm(forms.ModelForm):
    class Meta:
        model = Dietitian
        fields = [
            'first_name', 'middle_name', 'last_name', 'email', 'telephone_prefix', 'phone_number',
            'country_code_ios3', 'gender', 'password'
        ]
    def save(self, commit=True):
        dietitian = super(DietitianAddForm, self).save(commit=False)
        plain_password = self.cleaned_data['password']
        hashed_password = bcrypt.hashpw(plain_password.encode('utf-8'), bcrypt.gensalt())
        dietitian.password = hashed_password.decode('utf-8')
        dietitian.is_active = True
        if commit:
            dietitian.save()
        return dietitian

class DietitianUpdateForm(forms.ModelForm):
    class Meta:
        model = Dietitian
        fields = [
            'first_name', 'middle_name', 'last_name', 'email', 'telephone_prefix', 'phone_number',
            'country_code_ios3', 'gender'
        ]

class DietitianSignupForm(forms.ModelForm):
    class Meta:
        model = Dietitian
        fields = ['email', 'password']
