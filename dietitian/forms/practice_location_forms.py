from dietitian.models import PracticeLocation
from django import forms


class PracticeLocationAddForm(forms.ModelForm):
    class Meta:
        model = PracticeLocation
        fields = ['line1', 'line2', 'street', 'city', 'country', 'suburb', 'postcode', 'website']


class PracticeLocationUpdateForm(PracticeLocationAddForm):
    pass
