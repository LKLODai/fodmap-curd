from django.shortcuts import render, HttpResponse, redirect
from dietitian.forms import dietitian_forms, practice_location_forms
from dietitian.models import Dietitian, PracticeLocation
import bcrypt
from dietitian.decorators import dietitian_cookie_required


# Dietitian views
def dietitian_delete(request):
    id_ = request.GET.get('id_')
    Dietitian.objects.filter(id=id_).delete()
    return redirect('/dietitian/home/')

def dietitian_edit(request, id_):
    dietitian = Dietitian.objects.get(id=id_)
    if request.method == "GET":
        form = dietitian_forms.DietitianUpdateForm(instance=dietitian)
        return render(request, 'dietitian_edit.html', {'form': form})
    else:
        form = dietitian_forms.DietitianUpdateForm(request.POST, instance=dietitian)
        if form.is_valid():
            form.save()
            return render(request, 'dietitian_edit.html', {'form': form, 'update_success': True})
        else:
            print(form.errors)
            return render(request, 'dietitian_edit.html', {'form': form})

@dietitian_cookie_required
def dietitian_home(request):
    dietitian_form = Dietitian.objects.filter(id=request.COOKIES.get('dietitian'))
    practice_location_forms = PracticeLocation.objects.filter(
        dietitian_id=request.COOKIES.get('dietitian'))
    no_permission = False
    # if dietitian_form[0].is_active else True
    location_limit = True if practice_location_forms.count() >= 3 else False
    return render(request, 'dietitian_home.html', {'dietitian_form': dietitian_form, 'practice_location_forms': practice_location_forms, 'no_permission': no_permission, 'location_limit': location_limit})

def dietitian_login(request):
    if request.method == "GET":
        return render(request, 'dietitian_login.html')
    else:
        email = request.POST.get('username')
        password = request.POST.get('password')
        try:
            dietitian = Dietitian.objects.get(email=email)
        except Dietitian.DoesNotExist:
            return HttpResponse('User not found')
        if bcrypt.checkpw(password.encode('utf-8'), dietitian.password.encode('utf-8')):
            response = redirect('/dietitian/home/')
            response.set_cookie('dietitian', str(dietitian.id), max_age=3600)
            request.session['dietitian_id'] = {'dietitian_id': str(dietitian.id)}
            return response
        else:
            return redirect('/dietitian/login/')

def dietitian_logout(request):
    response = redirect('/home/')
    response.delete_cookie('dietitian')
    return response


def dietitian_signup(request):
    if request.method == "GET":
        form = dietitian_forms.DietitianAddForm()
        return render(request, 'dietitian_add.html', {'form': form})
    else:
        form = dietitian_forms.DietitianAddForm(request.POST)
        if form.is_valid():
            print(form.cleaned_data)
            form.save()
            return redirect('/dietitian/home/')
        else:
            print(form.errors)


# Home views
def home_page(request):
    dietitian = request.COOKIES.get('dietitian')
    practice_locations = PracticeLocation.objects.all().order_by('country')
    if dietitian:
        dietitian = Dietitian.objects.get(id=dietitian)
        return render(request, 'home_page.html', {'practice_locations': practice_locations, 'dietitian_name': dietitian.first_name })
    else:
        return render(request, 'home_page.html', {'practice_locations': practice_locations})

# Practice Location views
@dietitian_cookie_required
def practice_location_add(request):
    dietitian = request.COOKIES.get('dietitian')
    if request.method == "GET":
        form = practice_location_forms.PracticeLocationAddForm()
        return render(request, 'practice_location_add.html', {'form': form})
    else:
        form = practice_location_forms.PracticeLocationAddForm(request.POST)
        if form.is_valid():
            practice_location = form.save(commit=False)
            dietitian = Dietitian.objects.get(id=dietitian)
            practice_location.dietitian = dietitian
            practice_location.save()
            return redirect('/dietitian/home/')
        else:
            print(form.errors)

@dietitian_cookie_required
def practice_location_delete(request):
    id_ = request.GET.get('id_')
    PracticeLocation.objects.filter(id=id_).delete()
    return redirect('/dietitian/home/')

@dietitian_cookie_required
def practice_location_edit(request, id_):
    practice_location = PracticeLocation.objects.get(id=id_)
    if request.method == "GET":
        form = practice_location_forms.PracticeLocationUpdateForm(instance=practice_location)
        return render(request, 'practice_location_edit.html', {'form': form})
    else:
        form = practice_location_forms.PracticeLocationUpdateForm(request.POST, instance=practice_location)
        if form.is_valid():
            form.save()
            return render(request, 'practice_location_edit.html', {'form': form, 'update_success': True})
        else:
            print(form.errors)
            return render(request, 'practice_location_edit.html', {'form': form})
