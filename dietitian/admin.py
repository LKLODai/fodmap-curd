from django.contrib import admin

# Register your models here.

from django.contrib import admin
from .models import Dietitian, PracticeLocation

admin.site.register(Dietitian)
admin.site.register(PracticeLocation)
