# this is a practice project for FODMAP
# It supports dietitians to manage their practice location and display all the dietitians in home page

# To start the web
1. connect to database
put your personal db config into settings.py

2. create/active virtual environment
# create:
source create_venv.sh 
# active:
source enable_venv

3. qa tools
# run:
source check_my_code.sh  
# not developer yet :)

4. test unit test
# run:
python manage.py test

# All done!
