#!/bin/bash

ENV_NAME="venv"

python3 -m venv $ENV_NAME
source $ENV_NAME/bin/activate
pip3 install django==4.2.13
django-admin --version
pip3 install mysqlclient==2.2.4
pip3 install python-dotenv==1.0.1
pip3 install bcrypt==4.1.3

# pip3 install gdal==3.9.0
# pip3 install django[gis]

echo "env $ENV_NAME created and activated."
