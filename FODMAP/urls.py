"""
URL configuration for FODMAP project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from dietitian import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('dietitian/delete/', views.dietitian_delete),
    path('dietitian/home/', views.dietitian_home, name='dietitian_home'),
    path('dietitian/login/', views.dietitian_login, name='dietitian_login'),
    path('dietitian/logout/', views.dietitian_logout),
    path('dietitian/signup/', views.dietitian_signup, name='dietitian_signup'),
    path('dietitian/<uuid:id_>/edit/', views.dietitian_edit),
    path('home/', views.home_page, name='home_page'),
    path('practice_location/add/', views.practice_location_add),
    path('practice_location/delete/', views.practice_location_delete),
    path('practice_location/<uuid:id_>/edit/', views.practice_location_edit),
]
