#!/bin/bash

ENV_NAME="venv"
SETUP_SCRIPT="setup_django.sh"

if [ -d "$ENV_NAME" ]; then
    source $ENV_NAME/bin/activate
else
    if [ -f "$SETUP_SCRIPT" ]; then
        bash $SETUP_SCRIPT
    else
        exit 1
    fi
fi
